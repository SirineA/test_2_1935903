package planets;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {

	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		//creating a new collection of planets that we want to return
		Collection<Planet> newPlanets = new ArrayList<Planet>();
		//go through the collection of planets
		for(int i=0; i < planets.size(); i++) {
			//if their order is one add them to the newplanets
			if(((ArrayList<Planet>) planets).get(i).getOrder() == 1) {
				newPlanets.add(((ArrayList<Planet>) planets).get(i));
			}
			//if their order 2 add them to the newplanets
			else if(((ArrayList<Planet>) planets).get(i).getOrder() == 2) {
				newPlanets.add(((ArrayList<Planet>) planets).get(i));
			}
			//if their order is 3 add them to newplanets
			else if(((ArrayList<Planet>) planets).get(i).getOrder() == 3) {
				newPlanets.add(((ArrayList<Planet>) planets).get(i));
			}
		}
		return newPlanets;
	}
}
