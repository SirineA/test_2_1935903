import movies.importer.Movie;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	@Override
	public boolean equals(Object o) {
		//if o is not a planet, it should return false
		if(!(o instanceof Planet)) {
			return false;
		}
		//verifies if the planet has the same name and order, if not returns false
		if(((Planet)o).name.equals(this.name) && ((Planet)o).order == (this.order)){
				return true;
			}
		return this.name == ((Planet)o).name && this.order == ((Planet)o).order;
		
	}
	public int HashCode() {
		String combined = this.name+this.order;
		return combined.hashCode();
		
	}
	//comparing the name if its 0 then its the same name
	//comparing the system name if its 0 then it the same comparing the second to the first to make it decreasing
	public int compareTo(Planet p) {
		int comparison = this.getName().compareTo(p.getName());
		if(comparison == 0) {
			int planetarySystemComparison = p.getPlanetarySystemName().compareTo(this.getPlanetarySystemName());
		}
		return comparison;
	}
}
