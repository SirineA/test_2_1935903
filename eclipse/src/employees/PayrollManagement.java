package employees;

public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] employees = new Employee[6];
		employees[0]= new SalariedEmployee(15000);
		employees[1] = new HourlyEmployee(250);
		employees[2] = new UnionizedHourlyEmployee(270,5000);
		employees[3] = new HourlyEmployee(777); 
		employees[4] = new UnionizedHourlyEmployee(543,10000);
		employees[5] = new UnionizedHourlyEmployee(967,200);
		
		//the total expense should equal to 176 164$ if it right it will print the total expenses
		int totalExpenses = getTotalExpenses(employees);
		if(totalExpenses== 176164) {
		System.out.println(totalExpenses);}
	}

	public static int getTotalExpenses(Employee[] employees) {
		int totalExpenses = 0;
		for(int i = 0; i < employees.length; i++) {
			totalExpenses += employees[i].getYearlyPay();
		}
		return totalExpenses;
	}
}
