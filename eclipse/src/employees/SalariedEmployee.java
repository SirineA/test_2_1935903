package employees;

public class SalariedEmployee  implements Employee{

	private int yearlySalary;
	
	public SalariedEmployee(int salary) {
		this.yearlySalary = salary;
	}
	
	public int getYearlyPay() {
		return this.yearlySalary;
	}
}
