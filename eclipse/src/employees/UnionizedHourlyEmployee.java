package employees;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	
	private int pensionContribution;
	
	public UnionizedHourlyEmployee(int numOfHoursPerWeek, int pensionContribution) {
		super(numOfHoursPerWeek);
		this.pensionContribution = pensionContribution;
	}

	public int getYearlyPay() {
	int yearlyPay;
		int numOfWeekPerYear = 52;
		//multiplying the number of weeks in a year to the salary per week + the contribution
		yearlyPay = (getnumOfHoursPerWeek()*numOfWeekPerYear)+pensionContribution;
		return yearlyPay;
	}
}
