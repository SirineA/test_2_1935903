package employees;

public class HourlyEmployee implements Employee{
	
	private int numOfHoursPerWeek;
	
	public HourlyEmployee(int numOfHoursPerWeek) {
		this.numOfHoursPerWeek =numOfHoursPerWeek;
	}
	
	public int getnumOfHoursPerWeek() {
		return this.numOfHoursPerWeek;
	}
	public int getYearlyPay() {
		int yearlyPay;
		int numOfWeekPerYear = 52;
		//multiplying the number of weeks in a year to the salary per week
		yearlyPay = (this.numOfHoursPerWeek)*numOfWeekPerYear;
		return yearlyPay;
	}

}
